package repo

import (
	"log"
	"reflect"

	"gorm.io/gorm"
)

type Conn interface {
	GetConexao() (*gorm.DB, error)
	CloseConn(db *gorm.DB)
}

var conn Conn

func FindById(value interface{}, pk interface{}) *gorm.DB {

	if checkType(value, false) {
		db, _ := conn.GetConexao()
		defer conn.CloseConn(db)
		tx := db.Find(&value, pk)
		printErro(tx.Error)
		return tx
	}

	panic("o valor do método FindById deverá ser um ponteiro")

}

func Save(value interface{}) *gorm.DB {
	if checkType(value, false) {
		db, _ := conn.GetConexao()
		defer conn.CloseConn(db)
		tx := db.Save(value)
		printErro(tx.Error)
		return tx
	}
	panic("o valor do método Save deverá ser um ponteiro")
}

func FindAll(value interface{}) *gorm.DB {

	if checkType(value, true) {
		db, _ := conn.GetConexao()
		defer conn.CloseConn(db)
		var v = value
		tx := db.Find(v)
		printErro(tx.Error)
		return tx
	}
	panic("o valor do método FindAll deverá ser um ponteiro de slice")
}

func printErro(err error) {
	if err != nil {
		log.Println(err)
	}
}

func checkType(value interface{}, checkIsSlice bool) bool {

	rt := reflect.TypeOf(value)
	if rt.Kind() == reflect.Ptr {

		switch reflect.ValueOf(value).Elem().Kind() {
		case reflect.Slice:
			return checkIsSlice
		default:
			return !checkIsSlice
		}

	}
	return !checkIsSlice

}
