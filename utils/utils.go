package utils

import (
	"regexp"
	"strings"
	"unicode"

	"github.com/google/uuid"
	"golang.org/x/text/runes"
	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
)

func GerarUiid() string {
	uuidValue, _ := uuid.NewUUID()
	return uuidValue.String()
}

func RemoverAcentos(texto string) string {
	runesRemove := runes.Remove(runes.In(unicode.Mn))
	t := transform.Chain(norm.NFD, runesRemove, norm.NFC)
	result, _, _ := transform.String(t, texto)
	return result
}

func RemoverAcentosLC(texto string) string {
	return RemoverAcentos(strings.ToLower(texto))
}

func RemoverAcentosUC(texto string) string {
	return RemoverAcentos(strings.ToUpper(texto))
}

func SoNumeros(str string) bool {

	reg := `^[0-9]+$`
	return ValidarRegex(str, reg)
}

func ValidarRegex(str string, strRegex string) bool {
	re := regexp.MustCompile(strRegex)
	return re.MatchString(str)
}
