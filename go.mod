module bitbucket.org/kirmair/libsgo

go 1.17

replace bitbucket.org/kirmair/libsgo/webutils => ./webutils

replace bitbucket.org/kirmair/libsgo/utils => ./utils

replace bitbucket.org/kirmair/libsgo/repo => ./repo

require (
	github.com/google/uuid v1.3.0
	golang.org/x/text v0.3.7
)

require (
	bitbucket.org/kirmair/libsgo/repo v0.0.0-20211203010651-9eb65e9e03bb
	bitbucket.org/kirmair/libsgo/utils v0.0.0-20211203010651-9eb65e9e03bb
	bitbucket.org/kirmair/libsgo/webutils v0.0.0-20211203010651-9eb65e9e03bb
	github.com/jinzhu/inflection v1.0.0 // indirect
	github.com/jinzhu/now v1.1.3 // indirect
	gorm.io/gorm v1.22.4 // indirect
)
