package webutils

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
)

const (
	EXCEL_TYPE     = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
	JSON_UTF8_TYPE = "application/json;charset=UTF-8"
	JSON_TYPE      = "application/json"
	XML_TYPE       = "application/xml"

	HTTP_GET     = "GET"
	HTTP_POST    = "POST"
	HTTP_PUT     = "PUT"
	HTTP_DELETE  = "DELETE"
	HTTP_OPTIONS = "OPTIONS"

	REFRESH_TOKEN_VENCIDO = "REFRESH_TOKEN_VENCIDO"
	TOKEN_PRINCIPAL       = "PRINCIPAL"
	TOKEN_REFRESH         = "REFRESH"
	COOKIE_NAME           = "refreshToken"
	SERVIDOR_LOTADO       = "SERVIDOR_LOTADO"

	FISICA   = "FISICA"
	JURIDICA = "JURIDICA"
)

func SetCabecalhos(w http.ResponseWriter, origem []string) {
	if len(origem) == 1 {
		log.Println("Setando valor origem: " + origem[0])
		w.Header().Set("Access-Control-Allow-Origin", origem[0])
	}
	w.Header().Set("Content-Type", JSON_UTF8_TYPE)
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "Authorization, Content-Type, Accept, auth, token, schema")
	w.Header().Set("Cache-Control", "no-cache")
	w.Header().Set("Access-Control-Allow-Credentials", "true")
	w.Header().Set("Access-Control-Max-Age", "3600")
}

func CheckRegexUrl(link string, url string) (bool, []string) {

	re := regexp.MustCompile(link)

	resultado := re.FindStringSubmatch(url)

	if len(resultado) == 0 {
		return false, resultado
	} else if resultado[0] != url {
		return false, resultado
	} else {
		return true, resultado
	}
}

func ConverterJsonToStruts(body io.ReadCloser, value interface{}) error {

	bodyByte, err := ioutil.ReadAll(body)
	if err != nil {
		return err
	}
	err = json.Unmarshal(bodyByte, &value)
	return err

}
