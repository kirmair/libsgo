module bitbucket.org/kirmair/libsgo/webutils

go 1.17

require (
	github.com/google/uuid v1.3.0
	golang.org/x/text v0.3.7
)
