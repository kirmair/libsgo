package libsgo

import "time"

type DataHora time.Time
type Data time.Time

const (
	DateFormat = "2006-01-02"
	TimeFormat = "2006-01-02 15:04:05"

	YYYY_MM_DD = DateFormat
	HH_MM_SS   = TimeFormat
)

func (t *Data) UnmarshalJSON(data []byte) (err error) {
	now, err := time.ParseInLocation(`"`+DateFormat+`"`, string(data), time.Local)
	*t = Data(now)
	return
}

func FormatarDataHoraData(dt time.Time, format *string) *string {

	if format != nil {
		var nasc string = dt.Format(*format)
		return &nasc
	}

	var nasc string = dt.Format(DateFormat)
	return &nasc

}

func (t Data) MarshalJSON() ([]byte, error) {
	b := make([]byte, 0, len(TimeFormat)+2)
	b = append(b, '"')
	b = time.Time(t).AppendFormat(b, DateFormat)
	b = append(b, '"')
	return b, nil
}

func (t *DataHora) UnmarshalJSON(data []byte) (err error) {
	now, err := time.ParseInLocation(`"`+TimeFormat+`"`, string(data), time.Local)
	*t = DataHora(now)
	return
}

func (t DataHora) MarshalJSON() ([]byte, error) {
	b := make([]byte, 0, len(TimeFormat)+2)
	b = append(b, '"')
	b = time.Time(t).AppendFormat(b, TimeFormat)
	b = append(b, '"')
	return b, nil
}

func (t DataHora) GetTime() time.Time {
	return time.Time(t)
}

func (t Data) GetTime() time.Time {
	return time.Time(t)
}

func DataHoraNow() DataHora {
	return DataHora(time.Now())
}
